import turtle
import tkinter
from tkinter import *


win = Tk()
win.title("Dessin")
win.iconbitmap('img\\logo.ico')
win.geometry("100x100")
win.resizable(False, False)

turtle.bgcolor("white")
turtle.pencolor("red")
turtle.speed(4000)
for a in range(150):
	turtle.circle(60)
	turtle.forward(100)
	turtle.left(67)

turtle.speed(10)
turtle.left(45)
turtle.forward(250)

turtle.speed(4000)
turtle.pencolor("green")
for b in range(150):
	turtle.circle(60)
	turtle.forward(100)
	turtle.left(67)

turtle.speed(10)
turtle.left(170)
turtle.forward(255)

turtle.speed(4000)
turtle.pencolor("green")
for c in range(150):
	turtle.circle(60)
	turtle.forward(100)
	turtle.left(67)

turtle.speed(10)
turtle.left(145)
turtle.forward(275)

turtle.left(90)
turtle.pencolor("black")
turtle.forward(200)
turtle.left(45)
turtle.pencolor("black")
turtle.forward(50)
turtle.right(135)
turtle.pencolor("black")
turtle.forward(200)
turtle.right(135)
turtle.pencolor("black")
turtle.forward(50)
turtle.right(315)
turtle.pencolor("black")
turtle.forward(200)
turtle.right(90)
turtle.pencolor("black")
turtle.forward(140)


turtle.end_fill()
win.mainloop()


