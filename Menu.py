'''
Done by Gazzera Marco:
* Scripts in 1: 
                  Pong
                  Snake
                  Tournette
                  Turle Draw
                  Musics
                  Olde files
'''

'''
The import
'''
import tkinter
from tkinter import messagebox
from tkinter import *
import math
import random
import random
import sys, time, csv
import pygame
from pygame.locals import*
import datetime
from subprocess import call

'''
Beginning
To star the script
'''

win = Tk()
win.title("Mega Menu")
win.iconbitmap('img\\logo.ico')
win.geometry("500x500")
win.resizable(False, False)
win.configure(background='dark green')
label = Label(win, bg="red" ,text = "Bienvenue au menu des jeux \n et des musiques", font=("Courier", 15))
label.pack(pady = 50)
label1 = Label(win, bg = "red", text="Cliquez où vous voulez pour jouer \n ou écouter de la musique",font=("Courier", 15))
label1.pack(pady = 51)


'''
Script about Snake
'''
def snake():
    call(["python", "Snake.py"])

'''
Script about Pong
'''

def pong():
    call(["python", "Pong.py"])

'''
Script about Tournette
'''

def tournette():

    pygame.init()

    icon = pygame.image.load("logo.jpg")
    pygame.display.set_icon(icon)

    WIDTH, HEIGHT = 500, 500
    SIZE = 500 
    WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Tournette")

    angle_per_step = .5 
    step = 0

    while True:
        WINDOW.fill('purple') 
        angle = step * angle_per_step
        line_len = SIZE * .8
        cx = cy = SIZE // 2
        x = line_len/2.0*math.sin(angle)
        y = line_len/2.0*math.cos(angle)
  
        pygame.draw.line(WINDOW, (255,255,255), ((cx+x+40),(cy+y+40)),((cx-x-40),(cy-y-40)), 4)
        pygame.draw.line(WINDOW, (255,0,0), ((cx+x+30),(cy+y+30)),((cx-x-30),(cy-y-30)), 4)
        pygame.draw.line(WINDOW, (0,255,0), ((cx+x+20),(cy+y+20)),((cx-x-20),(cy-y-20)), 4)
        pygame.draw.line(WINDOW, (0,0,255), ((cx+x+10),(cy+y+10)),((cx-x-10),(cy-y-10)), 4)
        pygame.draw.line(WINDOW, (0,0,0), ((cx+x),(cy+y)),((cx-x),(cy-y)), 4)

        pygame.display.update()
        pygame.time.Clock().tick(60)

        if pygame.event.peek(pygame.QUIT):  
            break

        step += 1  
    pygame.quit()  

'''
Script about me
'''

def affiche():

  print("Gazzera Marco")

'''
Script about music
'''

def music():

    pygame.init()
    icon = pygame.image.load("logo.jpg")
    pygame.display.set_icon(icon)
    pygame.display.set_caption("Musique")

    WIDTH, HEIGHT = 500, 500
    WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))

    soundObj = pygame.mixer.Sound('some.wav')
    soundObj.play()
    time.sleep(300) 
    soundObj.stop()

    while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
            pygame.display.update()

def music2():
    
    pygame.init()
    icon = pygame.image.load("logo.jpg")
    pygame.display.set_icon(icon)
    pygame.display.set_caption("Musique")

    WIDTH, HEIGHT = 500, 500
    WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))

    soundObj = pygame.mixer.Sound('trailer.wav')
    soundObj.play()
    time.sleep(300) 
    soundObj.stop()

    while True:
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
            pygame.display.update()

'''
Script about draw 
'''
def draw():
    call(["python", "Draw.py"])
'''
Script open last score for snake
'''
def open_score_snake():
    with open("dernier_score_snake.txt", 'r') as score_snake:
        lines = score_snake.readlines()
        for line in lines:
            print(line)

'''
Script open last score for pong
'''
def open_score_pong():
    with open("dernier_score_pong.txt", 'r') as score_pong:
        lines = score_pong.readlines()
        for line in lines:
            print(line)
    
'''
Part of buttons
'''

btn1 = Button(
  win,
  text = "SNAKE",
  command = snake, 
  activeforeground = "green",
  activebackground = "red",
  padx = 8,
  pady = 5
)
btn2 = Button(
  win, 
  text = "PONG",
  command = pong,
  activeforeground = "black",
  activebackground = "orange",
  padx = 8,
  pady = 5
)
btn3 = Button(
  win,
  text =" Gazzera Marco",
  command = affiche,
  padx = 8,
  pady = 5
)
btn4 = Button(
  win, 
  text = "QUITTER",
  command = win.destroy,
  activeforeground = "white",
  activebackground = "black",
  padx = 8,
  pady = 5
)
btn5 = Button(
  win, 
  text = "TOURNETTE",
  command = tournette,
  activeforeground = "black",
  activebackground = "purple",
  padx = 8,
  pady = 5
)
btn010 = Button(
  win, 
  text = "DESSIN",
  command = draw,
  activeforeground = "green",
  activebackground = "black",
  padx = 8,
  pady = 5
)
btn6 = Button(
  win, 
  text = "MUSIQUE",
  command = music,
  activeforeground = "black",
  activebackground = "red",
  padx = 8,
  pady = 5
)
btn7 = Button(
  win, 
  text = "MUSIQUE JEUX",
  command = music2,
  activeforeground = "red",
  activebackground = "black",
  padx = 8,
  pady = 5
)
btn8 = Button(
  win, 
  text = "DERNIER SCORE\nSNAKE",
  command = open_score_snake,
  activeforeground = "green",
  activebackground = "black",
  padx = 8,
  pady = 5
)
btn9 = Button(
  win, 
  text = "DERNIER SCORE\nPONG",
  command = open_score_pong,
  activeforeground = "orange",
  activebackground = "purple",
  padx = 8,
  pady = 5
)

btn1.pack(side = LEFT)
btn8.pack(side = LEFT)
btn2.pack(side = RIGHT)
btn9.pack(side = RIGHT)
btn4.pack(side = BOTTOM)
btn3.pack(side = BOTTOM)
btn5.pack(side = BOTTOM)
btn010.pack(side = BOTTOM)
btn6.pack(side = TOP)
btn7.pack(side = TOP)


'''
End 
To finish the script
'''

win.mainloop()