'''
Done by Gazzera Marco:
* The initialisation for the Menu
'''

'''
The import
'''
from subprocess import call
from tkinter import ttk, Tk
from tkinter import *
from tkinter import messagebox

'''
Beginning
To star the script
'''

win  = Tk()
win.title("CONNECTION WINDOW")
win.iconbitmap('img\\logo.ico')
win.geometry("400x300")
win.resizable(False, False)
win.configure(background='dark green')

lbl_title = Label(win,borderwidth = 3, relief = SUNKEN, text = "Login form", font = ("Arial", 25), background = "red", foreground="white")
lbl_title.place(x = 25, y = 0, width = 350)

#For username
lbl_username = Label (win, text="Username :", font=("Arial", 14),bg="red", fg="white")
lbl_username.place(x = 5, y = 100,width = 150 )
txt_username = Entry(win,bd=4, font=("Arial", 13))
txt_username.place(x=150,y=100,width=200,height=30)

#For the password
lbl_passeword = Label (win, text="Password :", font=("Arial", 14),bg="red", fg="white")
lbl_passeword.place(x = 5, y = 150,width = 150 )
txt_passeword = Entry(win,show="*",bd=4, font=("Arial", 13))
txt_passeword.place(x=150,y=150,width=200,height=30)

'''
Part for login at the menu
'''

def login():
    user_name = txt_username.get()
    password = txt_passeword.get()
    if (user_name == "" and password == ""):
        messagebox.showerror("", "data must be entered")
        txt_passeword.delete("0", "end")
        txt_username.delete("0", "end")
    elif (user_name == "marco" and password == "03042000"):
        messagebox.showinfo("", "Welcome")
        txt_username.delete("0", "end")
        txt_passeword.delete("0", "end")
        win.destroy()
        call(["python", "Menu.py"])

    else:
        messagebox.showwarning("", "Erreur de Connexion")
        txt_passeword.delete("0", "end")
        txt_username.delete("0", "end")

#Button for login
btnenregistrer = Button(win, text = "Connexion", font = ("Arial", 16),bg ="red", fg = "white", command=login)
btnenregistrer.place(x=150, y= 225, width=200)

'''
End 
To finish the script
'''

win.mainloop()